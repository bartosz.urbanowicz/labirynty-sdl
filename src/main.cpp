#include <SDL.h>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>

const int M_WIDTH = 30, M_HEIGHT = 30;
const int WIDTH = M_WIDTH*20 + 60, HEIGHT = M_HEIGHT*20 + 60; 

struct cell 
    {
        bool U = 0;
        bool D = 0;
        bool L = 0;
        bool R = 0;
        bool visited = 0;
    };

cell grid [M_HEIGHT][M_WIDTH];

int current_x = 0;
int current_y = 0;

void renderAscii()
{
    for(int i = 0; i < M_WIDTH; i++)
    {
        std::cout << " _";
    }
    std::cout << std::endl;
    for(int i = 0; i < M_HEIGHT; i++)
    {
        std::cout << "|";
        for(int j = 0; j < M_WIDTH; j++)
        {
            if(grid[i][j].D == 0)
                std::cout << "_";
            else
                std::cout << " ";
            if(grid[i][j].R == 0)
                std::cout << "|";
            else
                std::cout << " ";
        }
        std::cout << std::endl;
    }
}

void renderMaze(SDL_Renderer *renderer)
{
    int r = rand() % 100;
    int g = rand() % 100;
    int b = rand() % 100;
    SDL_SetRenderDrawColor( renderer, 255, 255, 255, 255 );
    SDL_RenderClear( renderer );
    SDL_SetRenderDrawColor( renderer, r + 155, g + 155, b + 155, 255);
    SDL_RenderClear( renderer );

    SDL_Rect v_wall = { 30, 30, 3, 23 };
    SDL_Rect h_wall = { 30, 30, 20, 3 };
    SDL_SetRenderDrawColor( renderer, r, g, b, 255);        

    for(int i = 0; i < M_WIDTH; i++)
    {
        SDL_RenderFillRect( renderer, &h_wall ); 
        h_wall.x += 20;
    }
    h_wall.y += 20;
    h_wall.x = 30;
    for(int i = 0; i < M_HEIGHT; i++)
    {
        SDL_RenderFillRect( renderer, &v_wall );
        v_wall.x = 30 + 20;
        for(int j = 0; j < M_WIDTH; j++)
        {
            if(grid[i][j].D == 0)
                SDL_RenderFillRect( renderer, &h_wall );
            if(grid[i][j].R == 0)
                SDL_RenderFillRect( renderer, &v_wall );
            h_wall.x += 20;
            v_wall.x += 20;
        }
        h_wall.y += 20;
        v_wall.y += 20;
        h_wall.x = 30;
        v_wall.x = 30;
    }
    SDL_RenderPresent( renderer );
}

void move(int current_x, int current_y)
{
    std::vector <char> pos_directions;

    if(current_x > 0 && grid[current_x - 1][current_y].visited == 0)
        pos_directions.push_back('U');
    if(current_x < M_HEIGHT - 1 && grid[current_x + 1][current_y].visited == 0)
        pos_directions.push_back('D');
    if(current_y > 0 && grid[current_x][current_y - 1].visited == 0)
        pos_directions.push_back('L');
    if(current_y < M_WIDTH - 1 && grid[current_x][current_y + 1].visited == 0)
        pos_directions.push_back('R');

    if(pos_directions.size() == 0)
        return;
    else
    {
        
        char dir = pos_directions[rand() % pos_directions.size()];

        if(dir == 'U')
        {
            grid[current_x][current_y].U = 1;
            grid[current_x - 1][current_y].D = 1;
            current_x--;
        }
        if(dir == 'D')
        {
            grid[current_x][current_y].D = 1;
            grid[current_x + 1][current_y].U = 1;
            current_x++;
        }
        if(dir == 'L')
        {
            grid[current_x][current_y].L = 1;
            grid[current_x][current_y - 1].R = 1;
            current_y--;
        }
        if(dir == 'R')
        {
            grid[current_x][current_y].R = 1;
            grid[current_x][current_y + 1].L = 1;
            current_y++;
        }
        grid[current_x][current_y].visited = 1;
        move(current_x, current_y);
        move(current_x, current_y);
    }
    
}

int main(int argv, char** args)
{
    SDL_Init(SDL_INIT_EVERYTHING);

    SDL_Window *window = SDL_CreateWindow("bruh", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_ALLOW_HIGHDPI);
    if(NULL == window)
    {
        std::cout <<  "Could not create window: "  << SDL_GetError() << std::endl;
        return 1;
    }

    SDL_Renderer *renderer = SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED );
    if(NULL == renderer)
    {
        std::cout <<  "Could not create renderer: "  << SDL_GetError() << std::endl;
        return 1;
    }

    grid[0][0].visited = 1;
    srand(time(NULL));
    move(0,0);

    SDL_Event windowEvent;

    renderMaze(renderer);
    //renderAscii();

    while(true)
    {
        
        if(SDL_PollEvent(&windowEvent))
        {
            if(SDL_QUIT == windowEvent.type)
                break;
        }
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}